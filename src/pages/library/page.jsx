"use client";

import axios from "axios";
import Global from "../../../../spotify-demo/src/Global/Global"
export default function Page() {
  const access_token = localStorage.getItem('access_token');
  getUsuario();
  getListas();
  //getRecentPlaylist();

  async function getUsuario() {
    try {
      const headers = {
        headers: {
          "Authorization": "Bearer " + access_token
        }
      }

        axios.get("https://api.spotify.com/v1/me", headers).then(res => {
          const nombre = res.data.id
            console.log('nombre', nombre, 'res', res.data);
        })
    } catch (error) {
        console.log(error);
    }
  }

  async function getListas() {
    try {
      var offsetPlaylist =0;
      const headers = {
        headers: {
          "Authorization": "Bearer " + access_token
        }
      }

      axios.get("https://api.spotify.com/v1/me/playlists?limit=" + Global.playlistLimit + "&offset=" + offsetPlaylist + "", headers).then(response => {
          var datos = response.data;
          console.log('getListas',datos);
        })
    } catch (error) {
        console.log(error);
    }
  }

  /* async function getRecentPlaylist() {
      try {
        const headers = {
          headers: {
            "Authorization": "Bearer " + access_token
          }
        }
  
        axios.get("https://api.spotify.com/v1/me/player/recently-played", headers).then(response => {
            var datos = response.data;
            console.log('getRecentPlaylist',datos);
          })
      } catch (error) {
          console.log(error);
      }
  } */

  return (
    <div className="p-24">
      <h1>Hello, library!</h1>
    </div>
  ); 
}