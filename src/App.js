import logo from './logo.svg';
import './App.css';
import HeaderNav from "./components/header/header";
import "./home.css";
import CardGridManage from './components/cards/cardGridManage';

function App() {
  return (
    <main className="mainContent">
          <div className="capaOne"></div>
          <div className="capaTwo"></div>
          <div className="content">
            <HeaderNav></HeaderNav>
            <div className="contentMidCards">
              <h1>Good afternoon</h1>
              <CardGridManage></CardGridManage>
            </div>
          </div>
    </main>
  );
}

export default App;