"use client";

import axios from "axios";
import { useEffect } from "react";
import { useSearchParams, useRouter } from "next/navigation";
var scopes = "user-read-private user-read-email ugc-image-upload user-read-playback-state user-modify-playback-state user-read-currently-playing playlist-read-private playlist-read-collaborative user-read-recently-played";

const dev =
  `https://accounts.spotify.com/authorize?client_id=14f25584da2c4ed2b2eeb9b3a5d9b83b&response_type=code&redirect_uri=http://localhost:3000/login&scope=` +
  scopes;
const prod = `https://accounts.spotify.com/authorize?client_id=14f25584da2c4ed2b2eeb9b3a5d9b83b&response_type=code&redirect_uri=https://spotify-demo-andreemalerva.netlify.app/login&scope=user-read-private`;

export default function Login() {
  const router = useRouter();
  const searchParams = useSearchParams();

  useEffect(() => {
    const spotyCode = searchParams.get("code");
    if (spotyCode) {
      getAuth(spotyCode);
    }
  });

  async function getAuth(spotyCode: string) {
    const searchParamsDev = new URLSearchParams({
      code: spotyCode,
      grant_type: "authorization_code",
      redirect_uri: "http://localhost:3000/login",
      client_id: "14f25584da2c4ed2b2eeb9b3a5d9b83b",
      client_secret: "fd062f25fcb8454e8fe6dd83ac86cc56",
    });
    const searchParamsProd = new URLSearchParams({
      code: spotyCode,
      grant_type: "authorization_code",
      redirect_uri: "https://spotify-demo-andreemalerva.netlify.app/login",
      client_id: "14f25584da2c4ed2b2eeb9b3a5d9b83b",
      client_secret: "fd062f25fcb8454e8fe6dd83ac86cc56",
    });
    axios
      .post("https://accounts.spotify.com/api/token", searchParamsDev)
      .then(function (response) {
        console.log("spotyCode", spotyCode);
        localStorage.setItem("access_token", response.data.access_token);
        localStorage.setItem("refresh_token", response.data.refresh_token);
        router.push("/");
        //console.log(response);
        console.log("res", response);
      })
      .catch(function (error) {
        console.log(error);
      });
  }

  function login() {
    window.location.replace(dev);
  }

  return (
    <div className="general">
      <div id="login">
        <h3 className="subtitle">
          Visualiza toda la información de tu perfil de Spotify
        </h3>
        <button onClick={login} id="btnLogin" className="btnLogin">
          INICIAR SESION
        </button>
      </div>
    </div>
  );
}
