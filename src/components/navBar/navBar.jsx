"use client";

import styles from "./navBar.module.css";
import React, { useRef } from "react";

export default function NavBar() {
    const imageHome = useRef(null);
    const imageSearch = useRef(null);
    const imageLibrary = useRef(null);
    var imgHome ="../icons/home.svg";
    var imgSearch = "../icons/Search_S.svg";
    var imgLibrary = "../icons/Library_S.svg";
    var imgCreatePlaylist = "../icons/+Library_S.svg";
    var imgLiked ="../icons/Liked Songs_S.svg";

    const handleClick = (_Event, path) => {  
        if (path === "/") {
            imageHome.current.src = "../icons/Home_Fill_S.svg";
            imageSearch.current.src =  "../icons/Search_S.svg";
            imageLibrary.current.src = "../icons/Library_S.svg";
          }   
         if (path === "/search") {
            imageHome.current.src ="../icons/home.svg";
            imageSearch.current.src = "../icons/Search_Fill_S.svg";
            imageLibrary.current.src = "../icons/Library_S.svg";
         }
         if (path === "/library") {
            imageHome.current.src ="../icons/home.svg";
            imageLibrary.current.src = "../icons/Library_Fill_S.svg";
            imageSearch.current.src =  "../icons/Search_S.svg";
          }
       };


    return (
        <nav className={styles.burguerBox}>
            <a href="/" onClick={(e) => handleClick(e, "/")} className={styles.burguerItem}>
                <img className="home" ref={imageHome} src={imageHome.current?.src || imgHome} width={32} height={32} alt="home" />Home
            </a>
            <a href="/search" onClick={(e) => handleClick(e, "/search")} className={styles.burguerItem}>
                <img className="search" ref={imageSearch} src={imageSearch.current?.src || imgSearch} width={32} height={32} alt="search" />Search
            </a>
            <a href="/library" onClick={(e) => handleClick(e, "/library")} className={styles.burguerItem}>
                <img className="library" ref={imageLibrary} src={imageLibrary.current?.src || imgLibrary} width={32} height={32} alt="library" />Your Library
            </a>
            <a href="/createPlaylist" onClick={(e) => handleClick(e, "/createPlaylist")} className={styles.burguerItemCreate}>
                <img className="createPlaylist" src={imgCreatePlaylist} width={32} height={32} alt="library" />Create Playlist
            </a>
            <a href="/liked" onClick={(e) => handleClick(e, "/liked")} className={styles.burguerItem}>
                <img className="liked" src={imgLiked} width={32} height={32} alt="library" />Liked Songs
            </a>
        </nav>
    )
}